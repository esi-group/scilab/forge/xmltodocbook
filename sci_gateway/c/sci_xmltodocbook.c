/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2008 - INRIA - Sylvestre LEDRU
 *  Copyright (C) 2008 - INRIA - Vincent COUVERT
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "gw_xmltodocbook.h"
#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "MALLOC.h"

int sci_xmltodocbook(char *fname,unsigned long l)
{
    static int n1 = 0,m1 = 0;
    static int n2 = 0,m2 = 0;
    static size_t l1 = 0, l2 = 0;

    static char *sourceXML = NULL, *destXML = NULL;

    CheckRhs(2,2);
    CheckLhs(1,1);

    if (GetType(1) != sci_strings)
      {
        // Wrong type
        Scierror(999,_("%s: Wrong type for input argument #%d: String expected.\n"),fname,1);
        return 0;
      }
    
    GetRhsVar(1,STRING_DATATYPE,&m1,&n1,&l1);
    sourceXML = cstk(l1);
    
    if (GetType(2) != sci_strings)
      {
        // Wrong type
        Scierror(999,_("%s: Wrong type for input argument #%d: String expected.\n"),fname,2);
        return 0;
      }
    
    GetRhsVar(2,STRING_DATATYPE,&m2,&n2,&l2);
    destXML = cstk(l2);


    // Call Java to do the work
    xmltodocbook(sourceXML, destXML);

    LhsVar(1) = 0 ;
    C2F(putlhsvar)();
    return 0;
}
