//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2008 - INRIA - Sylvestre LEDRU
//  Copyright (C) 2008 - INRIA - Vincent COUVERT
//  Copyright (C) 2010-2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// =============================================================================

function builder_main()

  TOOLBOX_NAME = "xmltodocbook";
  TOOLBOX_TITLE = "Scilab XML help file to Docbook conversion tool";

  mode(-1);
  lines(0);


// Check Scilab's version
// =============================================================================
  try
    v = getversion("scilab");
  catch
    error(gettext("Scilab 5.3 or more is required."));
  end

  if v(2) < 3 then
    // new API in scilab 5.3
    error(gettext('Scilab 5.3 or more is required.'));
  end


// Check modules_manager module availability
// =============================================================================
  if ~isdef('tbx_build_loader') then
    error(msprintf(gettext('%s module not installed."), 'modules_manager'));
  end

  toolbox_dir = get_absolute_file_path("builder.sce");

  tbx_builder_macros(toolbox_dir);
  tbx_builder_src(toolbox_dir);
  tbx_builder_gateway(toolbox_dir);
  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
  tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
// =============================================================================
// This function creates a build.xml file for ant using Scilab javaclasspath
  function createbuildxml(pathtofile, projectname, description, libraryname)

    xmlcontents = [];
    xmlcontents($+1) = "<project name="""+projectname+""" default=""jar"">";
    xmlcontents($+1) = "    <description>";
    xmlcontents($+1) = "    " + description;
    xmlcontents($+1) = "    </description>";
    xmlcontents($+1) = "";
    xmlcontents($+1) = "    <!-- Where the module should be created (modules/xxx/jar) -->";
    xmlcontents($+1) = "    <property name=""build.jar.dir""  value=""jar/"" />";
    xmlcontents($+1) = "    <!-- The building directory -->";
    xmlcontents($+1) = "    <property name=""build.dir"" location=""build/""/>";
    xmlcontents($+1) = "    <!-- Where builded classes will be generated -->";
    xmlcontents($+1) = "    <property name=""classes.dir"" location=""${build.dir}/classes"" />";
    xmlcontents($+1) = "";
    xmlcontents($+1) = "    <property name=""library.name"" value="""+libraryname+""" />";
    xmlcontents($+1) = ""
    xmlcontents($+1) = "    <!-- Where we can find the sources -->";
    xmlcontents($+1) = "    <property name=""src.dir""  location=""src/java/"" />";

    xmlcontents($+1) = "    <path id=""compile.classpath"">";
    jcp = javaclasspath();
    for k=1:size(jcp, 1)
      xmlcontents($+1) = "        <pathelement location="""+jcp(k)+"""/>";
    end
    xmlcontents($+1) = "    </path>";

    xmlcontents($+1) = "    <!-- Create the jar -->";
    xmlcontents($+1) = "    <target name=""jar"" description=""Build the jar file"" depends=""compile"">";
    xmlcontents($+1) = "        <jar destfile=""${build.jar.dir}/${library.name}"" basedir=""${classes.dir}"">";
    xmlcontents($+1) = "            <manifest>";
    xmlcontents($+1) = "            </manifest>";
    xmlcontents($+1) = "        </jar>";
    xmlcontents($+1) = "    </target>";
    xmlcontents($+1) = "    <!-- Compile sources -->";
    xmlcontents($+1) = "    <target name=""compile"" description=""Build sources"" depends=""init"">";
    xmlcontents($+1) = "        <javac";
    xmlcontents($+1) = "        srcdir=""${src.dir}""";
    xmlcontents($+1) = "        destdir=""${classes.dir}""";
    xmlcontents($+1) = "        classpathref=""compile.classpath""";
    xmlcontents($+1) = "        deprecation=""on""";
    xmlcontents($+1) = "        debug=""off""";
    xmlcontents($+1) = "        verbose=""off""";
    xmlcontents($+1) = "        listfiles=""on""";
    xmlcontents($+1) = "        source=""5""";
    xmlcontents($+1) = "        />";
    xmlcontents($+1) = "    </target>";
    xmlcontents($+1) = "    <target name=""init"">";
    xmlcontents($+1) = "        <mkdir dir=""${classes.dir}""/>";
    xmlcontents($+1) = "        <mkdir dir=""${build.jar.dir}""/>";
    xmlcontents($+1) = "        <!-- Create the time stamp -->";
    xmlcontents($+1) = "        <tstamp/>";
    xmlcontents($+1) = "    </target>";
    xmlcontents($+1) = "</project>";
  
    mputl(xmlcontents, pathtofile + "build.xml");
  endfunction
// =============================================================================
  if getos() == 'Windows' then
    JAVA_HOME = SCI + '\java\jdk';
    JDK_PATH = SCI + '\java\jdk';
    JRE_PATH = SCI + '\java\jre';
    ANT_PATH = SCI + '\java\ant'

    if ~isdir(JDK_PATH) then
      error("JDK could not be found on your system.");
    end

    if ~isdir(ANT_PATH) then
      error("ANT could not be found on your system.");
    end

    if ~isdir(JRE_PATH) then
      error("JRE could not be found on your system.");
    end
  
    PATH = getenv('PATH');
  
    setenv('JAVA_HOME', JAVA_HOME);
    setenv('JDK_PATH', JDK_PATH);
    setenv('JRE_PATH', JRE_PATH);
    setenv('ANT_PATH', ANT_PATH);
    setenv('PATH',ANT_PATH + '\bin;' + JDK_PATH + ';' + JRE_PATH + ';'+ PATH);
  end
// =============================================================================
// Compile Java file using ant
  if unix("ant -version") <> 0 then
    error("ant could not be found on your system.");
  end

  // Fake call to buildDoc so that optional jars are loaded
  execstr("buildDoc(1,2,3,4,5)","errcatch");
  // Create the build.xml file
  createbuildxml(toolbox_dir, TOOLBOX_NAME, ..
               "Build the XMLTODOCBOOK java class", ..
               "org.scilab.toolboxes.xmltodocbook.jar");

  // Compile
  cd(toolbox_dir);
  unix("ant");

  tbx_builder_help(toolbox_dir);
endfunction

builder_main();
clear builder_main;
