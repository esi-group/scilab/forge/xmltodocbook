//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2008 - INRIA - Vincent COUVERT
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

function xmltodocbook(sourceXML, outputXML, outputDir)

rhs = argn(2);

if rhs<>2 & rhs<> 3 then
  error(msprintf("%s: Wrong number of input argument(s): %d or %d expected.\n", "xmltodocbook", 2, 3)); 
end

sourceXML = fullpath(sourceXML);

sourcePath = fileparts(sourceXML, "path");

if argn(2) == 3 then
  outputXML = fullpath(outputDir) + filesep() + fileparts(outputXML, "fname") + fileparts(outputXML, "extension");
else
  outputXML = sourcePath + fileparts(outputXML, "fname") + fileparts(outputXML, "extension");
end

XMLConvert5(sourceXML, outputXML);

endfunction
