// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
create_refs = %f;
// ====================================================================
current_path = pwd();
root_path = get_absolute_file_path('tests_xmltodocbook.sce');
exec (root_path + 'loader.sce');
 setenv('TOOLBOX_XMLTODOCBOOK', root_path);
 // ====================================================================
 if create_refs then
   test_run(root_path, "xmltodocbook", "create_ref");
 else
   test_run(root_path);
 end
 cd(current_path);
 // ====================================================================
 clear current_path;
 clear root_path;
 // ====================================================================
