XMLTODOCBOOK toolbox
====================

This toolbox provides functions to help you to convert your old Scilab XML help files to the new Docbook based format.

To build this toolbox on Windows, you need to have ANT, JDK
example set PATH=%PATH%;D:\java\ant\bin;D:\java\jdk\bin; 
 