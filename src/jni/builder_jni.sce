//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2008 - INRIA - Sylvestre LEDRU
//  Copyright (C) 2008 - INRIA - Vincent COUVERT
//  Copyright (C) 2010-2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

function builder_jni()
  currentpath = get_absolute_file_path("builder_jni.sce");
  
  jni_files = ["xmltodocbook.cpp", "XMLConvert5.cpp", "GiwsException.cpp"];
  table_files = ["xmltodocbook", "XMLConvert5"];
  CFLAGS = "";
  
  
  if getos() == 'Windows' then
    currentpath = strsubst(currentpath,"\","/");
    // ticket 414 (remove optimization)
    CFLAGS = "/Od /TP";
  else
    // Toolbox embedded includes: Java and GIWS
    CFLAGS = " -I" + currentpath + "../../includes/";
  
    // Source tree version
    if isdir(SCI+"/modules/core/includes/") then
      CFLAGS = CFLAGS + " -I" + SCI + "/modules/jvm/includes/ -I" + SCI + "/modules/localization/includes/";
    end
  
    // Binary version
    if isdir(SCI+"/../../include/scilab/core/") then
      CFLAGS = CFLAGS + " -I" + SCI + "/../../include/scilab/jvm/ -I" + SCI + "/../../include/scilab/localization/";
    end
  
    // System version (ie: /usr/include/scilab/)	
    if isdir("/usr/include/scilab/") then
      CFLAGS = CFLAGS + " -I/usr/include/scilab/jvm/ -I/usr/include/scilab/localization/";
    end
  end
  
  CFLAGS = CFLAGS + " -I" + currentpath;
  
  tbx_build_src(table_files, jni_files, "c", currentpath, "", "", CFLAGS);
endfunction

builder_jni();
clear builder_jni;
