// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
tlbxs = atomsGetInstalled();
if grep(tlbxs, 'xmltodocbook') <> [] then
  atomsLoad("xmltodocbook");
  xmltodocbookref = tlbxs(grep(tlbxs, 'xmltodocbook'));
  pathtlbx = xmltodocbookref(3) + filesep();
else
  root_path = getenv('TOOLBOX_XMLTODOCBOOK','');
  if root_path <> '' then
    exec(root_path + 'loader.sce');
  end
  pathtlbx = root_path;
end
// ====================================================================
// <-- NO CHECK ERROR OUTPUT --> 
// <-- ENGLISH IMPOSED --> 
// ====================================================================
filenameToConvert = 'strings.xml';
fullfilenameToConvert = pathtlbx + 'examples' + filesep() + filenameToConvert;
copyfile(fullfilenameToConvert, TMPDIR);
cd TMPDIR;
xmltodocbook(filenameToConvert , "strings_converted.xml");
if ~isfile("strings_converted.xml") then pause,end
mdelete(TMPDIR + filesep() + filenameToConvert);
// ====================================================================
filenameToConvert = 'legendre.xml';
fullfilenameToConvert = pathtlbx + 'examples' + filesep() + filenameToConvert;
copyfile(fullfilenameToConvert, TMPDIR);
cd TMPDIR;
xmltodocbook(filenameToConvert , "legendre_converted.xml");
if ~isfile("strings_converted.xml") then pause,end
mdelete(TMPDIR + filesep() + filenameToConvert);
// ====================================================================
ierr = execstr("r = xmltojar(TMPDIR, ""tests_conversion"");", "errcatch");
if ierr <> 0 then pause,end
if ~isfile(r) then pause, end
// ====================================================================



